import rospy
import time

rospy.init_node('sound-source')


from geometry_msgs.msg import Twist
from odas_msgs.msg import odas, odassrc


import numpy as np

angle = 0

m = Twist()

m_pub = rospy.Publisher('/mobile_base/commands/velocity', Twist)


m_src = 0

def cb_odas(msg):
    global angle, m, m_pub, m_src
    for src in msg.src:
        # print "src", src
        if src.id != 0 and m_src == 0:
            # print "src", src
            m_src = src.id
            angle_ = np.arctan(src.y/src.x)
            gain = 1.0
            dangle = gain * (angle_ - angle)
            # angle += dangle
            m.angular.z = np.sign(dangle) * 0.6
            print "dangle", dangle, m
            angle = 0 # angle_
            break
        m_src = 0

    m_pub.publish(m)

    m.angular.z *= 0.5

odassub = rospy.Subscriber('/odas', odas, cb_odas)

while not rospy.is_shutdown():
    rospy.spin()

